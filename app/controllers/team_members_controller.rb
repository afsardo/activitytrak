class TeamMembersController < ApplicationController
  respond_to :html, :json
  before_action :set_team_member, only: [:show, :destroy]

  def show
    @team_member.status = 1
    @team_member.date = Time.now
    @team_member.save
    @event = Event.new(user_id: @current_user.id, object_type: 5, object_id: @team_member.id, action: "#{@current_user.name} joined a team!")
    @event.save
    redirect_to teams_path
  end

  def new
    session[:return_to] ||= request.referer
    @team = Team.find(params[:id])
    @team_member = TeamMember.new
    @team_member.team_id = params[:id]
    respond_with(@team_member)
  end

  def create
    @team = Team.find(params[:id])
    @team_member = TeamMember.new(team_member_params)
    @team_member.team_id = params[:id]
    @team_member.status = 0
    @team_member.save

    if @team_member.save
      redirect_to session.delete(:return_to)
      return nil
    else
      render :action => :new
      return nil
    end
  end

  def destroy
    team = Team.find(@team_member.team_id)
    @team_member.destroy
    redirect_to teams_path
  end

  private
    def set_team_member
      @team_member = TeamMember.find(params[:id])
    end

    def team_member_params
      params.require(:team_member).permit(:team_id, :user_id)
    end
end
