class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.string :name, null: false
      t.datetime :start_date
      t.datetime :final_date
      t.integer :max_tries, null: false
      t.integer :user_id, null: false
      t.integer :place_id, null: false
      t.integer :activity_type_id, null: false

      t.timestamps
    end

    add_index :challenges, [:activity_type_id, :user_id, :place_id]
  end
end
