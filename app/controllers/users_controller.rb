class UsersController < ApplicationController
    respond_to :html, :json
    skip_before_action :has_account, only: [:new, :create]
    skip_before_action :is_logged_in, only: [:show]
    before_action :already_has_account, only: [:new, :create]
    before_action :set_user, only: [:show, :edit, :update, :destroy]

    # SHOW USERS
    def index

    end

    # DASHBOARD PAGE
    def dashboard
      first = Date.today.beginning_of_month.beginning_of_week(:monday)
      last = Date.today.end_of_month.end_of_week(:monday)
      @calendar = (first..last).to_a.in_groups_of(7)
    end

    # PROFILE PAGE
    def show

    end

    # ACCOUNT SETTINGS
    def edit

    end

    def update
      @user.update(user_params)
      respond_with(@user)
    end

    # CREATE NEW USER AFTER REGISTRATION
    def new
      @user = User.new
      respond_with(@user)
    end

    # POST VALUES FROM CREATE NEW USER AFTER REGISTRATION
    def create
      @user = User.new(user_params)
      @user.login_id = current_login.id;
      @user.access_level = 1;
      @user.save

      if @user.save
        redirect_to root_path
      else
        respond_with(@user)
      end
    end

    private
      def set_user
        @user = User.find_by_profile_link(params[:id])
        unless @user
          @user = User.find(params[:id])
        end
      end

      def user_params
        params.require(:user).permit(:name, :birth_date, :gender, :weight, :height, :profile_link, :photo, :town)
      end

      def already_has_account
        if current_login.user
          redirect_to root_path
        end
      end

end
