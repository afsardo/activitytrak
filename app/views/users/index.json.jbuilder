json.array!(@users) do |user|
  json.extract! user, :id, :name, :photo, :birth_date, :town, :gender, :weight, :height, :profile_link, :access_level, :login_id
  json.url user_url(user, format: :json)
end
