class AdminController < ApplicationController

  before_action :is_admin

  def index

  end

  private

  def is_admin
    if login_signed_in?
      unless current_login.user.isAdmin?
        redirect_to access_denied_path
      end
    else
      redirect_to access_denied_path
    end
  end
end