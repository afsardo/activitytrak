class ChallengesController < ApplicationController
  respond_to :html, :json
  before_action :set_challenge, only: [:show, :edit, :update, :destroy]
  before_action :has_access, only: [:edit, :destroy, :update]

  def index
    @challenges = @current_user.challenges
    @challenges_pending = @current_user.challenges_pending
    respond_with(@challenges,@challenges_pending)
  end

  def show
    respond_with(@challenge)
  end

  def new
    @challenge = Challenge.new
    respond_with(@challenge)
  end

  def edit
  end

  def create
    @challenge = Challenge.new(challenge_params)
    @challenge.user_id = @current_user.id
    @challenge.save

    if @challenge.save
      challenger = Challenger.new
      challenger.user_id = @current_user.id
      challenger.challenge_id = @challenge.id
      challenger.status = 1
      challenger.save
      @event = Event.new(user_id: @current_user.id, object_type: 3, object_id: @challenge.id, action: "#{@current_user.name} created a challenge!")
      @event.save
    end

    respond_with(@challenge)
  end

  def update
    @challenge.update(challenge_params)
    respond_with(@challenge)
  end

  def destroy
    @challenge.destroy
    respond_with(@challenge)
  end

  private
    def has_access
      unless @challenge.user.id==@current_user.id
        redirect_to access_denied_path
      end
    end

    def set_challenge
      @challenge = Challenge.find(params[:id])
    end

    def challenge_params
      params.require(:challenge).permit(:name, :start_date, :final_date, :max_tries, :place_id, :activity_type_id)
    end
end
