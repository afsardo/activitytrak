class Activity < ActiveRecord::Base
  belongs_to :activity_type
  belongs_to :place
  belongs_to :user
  belongs_to :challenge

  validates :name, :activity_type_id, :place_id, :start_date, :final_date, presence: true
end
