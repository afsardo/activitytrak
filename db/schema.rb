# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141216055232) do

  create_table "activities", force: true do |t|
    t.string   "name",             null: false
    t.integer  "activity_type_id", null: false
    t.integer  "user_id",          null: false
    t.integer  "place_id",         null: false
    t.datetime "start_date"
    t.datetime "final_date"
    t.text     "json_object"
    t.integer  "challenge_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["activity_type_id", "user_id", "place_id"], name: "index_activities_on_activity_type_id_and_user_id_and_place_id"

  create_table "activity_types", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "challengers", force: true do |t|
    t.integer  "challenge_id",             null: false
    t.integer  "user_id",                  null: false
    t.integer  "status",       default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "challengers", ["challenge_id", "user_id"], name: "index_challengers_on_challenge_id_and_user_id"

  create_table "challenges", force: true do |t|
    t.string   "name",             null: false
    t.datetime "start_date"
    t.datetime "final_date"
    t.integer  "max_tries",        null: false
    t.integer  "user_id",          null: false
    t.integer  "place_id",         null: false
    t.integer  "activity_type_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "challenges", ["activity_type_id", "user_id", "place_id"], name: "index_challenges_on_activity_type_id_and_user_id_and_place_id"

  create_table "events", force: true do |t|
    t.integer  "object_type", null: false
    t.integer  "object_id",   null: false
    t.integer  "user_id",     null: false
    t.string   "action",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "events", ["user_id", "object_id"], name: "index_events_on_user_id_and_object_id"

  create_table "friendships", force: true do |t|
    t.integer  "user_id",                                    null: false
    t.integer  "friend_id",                                  null: false
    t.integer  "status",     default: 0
    t.datetime "date",       default: '2014-11-13 03:26:35'
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "friendships", ["user_id", "friend_id"], name: "index_friendships_on_user_id_and_friend_id"

  create_table "logins", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "gacc"
  end

  add_index "logins", ["email"], name: "index_logins_on_email", unique: true
  add_index "logins", ["reset_password_token"], name: "index_logins_on_reset_password_token", unique: true

  create_table "places", force: true do |t|
    t.string   "name",            null: false
    t.decimal  "coordinate_lat",  null: false
    t.decimal  "coordinate_long", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "team_members", force: true do |t|
    t.integer  "team_id",                                    null: false
    t.integer  "user_id",                                    null: false
    t.integer  "status",     default: 0
    t.datetime "date",       default: '2014-11-13 03:26:35'
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "team_members", ["team_id", "user_id"], name: "index_team_members_on_team_id_and_user_id"

  create_table "teams", force: true do |t|
    t.string   "name",       null: false
    t.string   "tag",        null: false
    t.string   "logo"
    t.integer  "user_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "teams", ["user_id"], name: "index_teams_on_user_id"

  create_table "users", force: true do |t|
    t.string   "name",                     null: false
    t.string   "photo"
    t.date     "birth_date",               null: false
    t.string   "town"
    t.string   "gender",                   null: false
    t.decimal  "weight",                   null: false
    t.decimal  "height",                   null: false
    t.string   "profile_link"
    t.integer  "access_level", default: 1
    t.integer  "login_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["login_id"], name: "index_users_on_login_id", unique: true

end
