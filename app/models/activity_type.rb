class ActivityType < ActiveRecord::Base
  has_many :activities, dependent: :destroy
  has_many :challenges, dependent: :destroy
  has_many :places, through: :activities
  has_many :users, through: :activities

  validates :name, presence: true
  validates :name, uniqueness: true

  scope :popular_sports, -> { select("activity_types.id, activity_types.name, count(activities.id) AS activities_count").joins(:activities).group("activity_types.id").order("activities_count DESC").limit(5) }

  def popular_places
    return self.places
  end
end
