Rails.application.routes.draw do
  namespace :admin do
    resources :users

    resources :friendships

    resources :challengers

    resources :challenges

    resources :places

    resources :activities

    resources :activity_types

    resources :team_members

    resources :teams
  end

  devise_for :logins, controllers: {
      sessions: 'login/sessions',
      registrations: 'login/registrations',
      passwords: 'login/passwords'
  }

  resources :activities
  resources :challenges
  resources :users, :except => [:index, :delete]
  resources :places, :except => [:edit, :update, :delete]
  resources :friendships, :except => [:new, :update]
  resources :teams
  resources :team_members, :except => [:index, :new, :create, :edit, :update]
  get 'team_member/new/:id' => 'team_members#new', as: 'new_team_member'
  post 'team_member/new/:id' => 'team_members#create', as: 'team_members'
  resources :challengers, :except => [:index, :new, :create, :edit, :update]
  get 'challenger/new/:id' => 'challengers#new', as: 'new_challenger'
  post 'challenger/new/:id' => 'challengers#create', as: 'challengers'

  get 'search' => 'welcome#search', as: 'search'
  get 'dashboard' => 'users#dashboard', as: 'dashboard'
  get 'livefeed' => 'welcome#livefeed', as: 'livefeed'
  get 'leaderboards' => 'welcome#leaderboards'
  get 'popular_sports' => 'welcome#popular_sports'
  get 'popular_places' => 'welcome#popular_places'
  get 'about' => 'welcome#about'
  get 'privacy' => 'welcome#privacy'
  get 'contact' => 'welcome#contact'
  get 'access_denied' => 'welcome#access_denied', as: 'access_denied'
  get 'not_found' => 'welcome#not_found', as: 'not_found'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # get 'admin/users' => 'admin/users#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
