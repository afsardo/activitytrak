class Team < ActiveRecord::Base
  belongs_to :user
  has_many :team_members, dependent: :destroy
  has_many :members, -> { where('team_members.status = 1') }, through: :team_members, source: :user
  has_many :activities, -> { order(final_date: :desc) }, through: :members

  validates :name, :tag, uniqueness: true
  validates :name, :tag, presence: true

  scope :leaderboard, -> { select("teams.id, teams.name, count(activities.id) AS activities_count").joins(:activities).group("teams.id").order("activities_count DESC").limit(15) }

  def isMember?(user)
    member = TeamMember.where(user_id: user.id, team_id: self.id, status: 1).first
    return member
  end

  def isPendingMember?(user)
    member = TeamMember.where(user_id: user.id, team_id: self.id, status: 0).first
    return member
  end
end
