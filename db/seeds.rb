# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Login.delete_all
lt = Login.create(email: 'test@test.com', password: 'test123', password_confirmation: 'test123')
la1 = Login.create(email: 'danresf@gmail.com', password: 'danresf123', password_confirmation: 'danresf123')
la2 = Login.create(email: 'tonyramos237@hotmail.com', password: 'tonyramos237', password_confirmation: 'tonyramos237')
l1 = Login.create(email: 'john@doe.com', password: 'johnDoe123', password_confirmation: 'johnDoe123')
l2 = Login.create(email: 'jane@doe.com', password: 'janeDoe123', password_confirmation: 'janeDoe123')
l3 = Login.create(email: 'bruce@alzheimer.com', password: 'bruceAlzheimer', password_confirmation: 'bruceAlzheimer')
l4 = Login.create(email: 'zack@parkinson.com', password: 'zackParkinson', password_confirmation: 'zackParkinson')
l5 = Login.create(email: 'allison@diabetes.com', password: 'allisonDiabetes', password_confirmation: 'allisonDiabetes')

User.delete_all
admin1 = User.create(name: 'Andre Sardo', birth_date: '1993-09-24', height: 1.75, weight: 74.8, gender: 'Male', photo: "https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xpf1/v/t1.0-9/310374_494598603902299_1012152063_n.jpg?oh=fcba9af2e13251af93a2c8355e077dbb&oe=550C3FD9&__gda__=1427131536_0742678204a35669a6d299313e77573c",
                     access_level: 2, login_id: la1.id)
admin2 = User.create(name: 'Pedro Mauricio', birth_date: '1992-12-13', height: 1.64, weight: 72.0, gender: 'Male', photo: "https://scontent-b-lhr.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/391388_488207287875774_709678035_n.jpg?oh=b90681f15ad734461349cf0caf2ce2fa&oe=54F8F81A",
                     access_level: 2, login_id: la2.id)
u1 = User.create(name: 'John Doe', birth_date: '1990-05-14', height: 1.80, weight: 72.5, gender: 'Male', login_id: l1.id)
u2 = User.create(name: 'Jane Doe', birth_date: '1993-02-24', height: 1.65, weight: 52.3, gender: 'Female', login_id: l2.id)
u3 = User.create(name: 'Bruce Alzheimer', birth_date: '1989-10-07', height: 1.70, weight: 65.8, gender: 'Male', login_id: l3.id, profile_link: 'brucez0r')
u4 = User.create(name: 'Zack Parkinson', birth_date: '1992-01-09', height: 1.72, weight: 80.1, gender: 'Male', login_id: l4.id)
u5 = User.create(name: 'Allison Diabetes', birth_date: '1996-07-18', height: 1.60, weight: 48.7, gender: 'Female', login_id: l5.id)

Friendship.delete_all
Friendship.create(user_id: u1.id, friend_id: u2.id, status: 1)
Friendship.create(user_id: u2.id, friend_id: u1.id, status: 0)
Friendship.create(user_id: u3.id, friend_id: u4.id, status: 2)
Friendship.create(user_id: u4.id, friend_id: u3.id, status: 2)

Team.delete_all
t1 = Team.create(name: 'Karate Team', tag: 'KT', user_id: u3.id)

TeamMember.delete_all
tm1 = TeamMember.create(team_id: t1.id, user_id: u3.id, status: 1)
tm2 = TeamMember.create(team_id: t1.id, user_id: u4.id, status: 0)

ActivityType.delete_all
at1 = ActivityType.create(name: 'Gym')
at2 = ActivityType.create(name: 'Running')
at3 = ActivityType.create(name: 'Cycling')
at4 = ActivityType.create(name: 'Football')
at5 = ActivityType.create(name: 'Basketball')

Place.delete_all
p1 = Place.create(name: 'Campo de Futebol FCT', coordinate_lat: 38.658515, coordinate_long: -9.204727)
p2 = Place.create(name: 'Mata da FCT', coordinate_lat: 38.657947, coordinate_long: -9.206160)

Challenge.delete_all
c1 = Challenge.create(name: 'MVP da Jogatana', start_date: '2014-11-10 12:30:02', final_date: '2014-11-14 19:14:36', max_tries: 1, user_id: u3.id, place_id: p1.id, activity_type_id: at4.id)

Challenger.delete_all
cr1 = Challenger.create(challenge_id: c1.id, user_id: u3.id, status: 1)
cr2 = Challenger.create(challenge_id: c1.id, user_id: u4.id, status: 1)

Activity.delete_all
js_obj = '{"distance": 10254,"heart_rate": 190}'
a1 = Activity.create(name: 'Corridinha Matinal', activity_type_id: at2.id, user_id: u4.id, place_id: p2.id, start_date: '2014-11-13 07:37:48', final_date: '2014-11-13 08:23:57', json_object: js_obj)
js_obj = '{"distance": 2030,"goals": 2,"faults": 5,"defenses": 0}'
a2 = Activity.create(name: 'Jogatana à Tarde', activity_type_id: at4.id, user_id: u3.id, place_id: p1.id, start_date: '2014-11-12 14:30:48', final_date: '2014-11-12 15:50:57', json_object: js_obj, challenge_id: c1.id)
js_obj = '{"distance": 3030,"goals": 1,"faults": 2,"defenses": 10}'
a3 = Activity.create(name: 'Jogatana à Tarde', activity_type_id: at4.id, user_id: u4.id, place_id: p1.id, start_date: '2014-11-12 14:30:48', final_date: '2014-11-12 15:50:57', json_object: js_obj, challenge_id: c1.id)


Event.delete_all
# Events object_type code
# 1: activities, 2: teams, 3: challenges, 4: challengers, 5: team_members