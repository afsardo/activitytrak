class ChallengersController < ApplicationController
  respond_to :html, :json
  before_action :set_challenger, only: [:show, :destroy]

  def show
    @challenger.status = 1
    @challenger.save
    @event = Event.new(user_id: @current_user.id, object_type: 4, object_id: @challenger.id, action: "#{@current_user.name} accepted a challenge!")
    @event.save
    redirect_to challenges_path
  end

  def new
    session[:return_to] ||= request.referer
    @challenge = Challenge.find(params[:id])
    @challenger = Challenger.new
    respond_with(@challenger)
  end

  def create
    @challenge = Challenge.find(params[:id])
    @challenger = Challenger.new(challenger_params)
    @challenger.challenge_id = params[:id]
    @challenger.status = 0
    @challenger.save

    if @challenger.save
      redirect_to session.delete(:return_to)
      return nil
    else
      render :action => :new
      return nil
    end
  end

  def destroy
    challenge = Challenge.find(@challenger.challenge_id)
    @challenger.destroy
    redirect_to challenges_path
  end

  private
    def set_challenger
      @challenger = Challenger.find(params[:id])
    end

    def challenger_params
      params.require(:challenger).permit(:challenge_id, :user_id, :status)
    end
end
