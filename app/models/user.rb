class User < ActiveRecord::Base
  belongs_to :login, dependent: :destroy
  has_many :friendships, dependent: :destroy
  has_many :friends, -> { where('friendships.status = 2') }, through: :friendships, source: :friend
  has_many :requested_friends, -> { where('friendships.status = 1') }, through: :friendships, source: :friend
  has_many :pending_friends, -> { where('friendships.status = 0') }, through: :friendships, source: :friend
  has_many :own_teams, class_name: 'Team', foreign_key: 'user_id'
  has_many :team_memberships, -> { where('team_members.status = 1') }, class_name: 'TeamMember', foreign_key: 'user_id'
  has_many :team_memberships_pending, -> { where('team_members.status = 0') }, class_name: 'TeamMember', foreign_key: 'user_id'
  has_many :teams, through: :team_memberships, source: :team
  has_many :teams_pending, through: :team_memberships_pending, source: :team
  has_many :activities, -> { order(final_date: :desc) }
  has_many :friends_activities, through: :friends, source: :activities
  has_many :places, through: :activities
  has_many :activity_types, through: :activities
  has_many :own_challenges, class_name: 'Challenge', foreign_key: 'user_id'
  has_many :challenges_participations, -> { where('challengers.status = 1') }, class_name: 'Challenger', foreign_key: 'user_id'
  has_many :challenges_participations_pending, -> { where('challengers.status = 0') }, class_name: 'Challenger', foreign_key: 'user_id'
  has_many :challenges, through: :challenges_participations, source: :challenge
  has_many :challenges_pending, through: :challenges_participations_pending, source: :challenge
  has_many :friend_activities, through: :friends, source: :activities
  has_many :friend_own_teams, through: :friends, source: :own_teams
  has_many :friend_teams, through: :friends, source: :team_memberships
  has_many :friend_own_challenges, through: :friends, source: :own_challenges
  has_many :friend_challenges, through: :friends, source: :challenges_participations

  validates :name, :gender, :weight, :height, :birth_date, presence: true
  validates :weight, :height, numericality: { greater_than_or_equal_to: 0 }
  validates :name, uniqueness: true

  scope :leaderboard, -> { select("users.id, users.name, count(activities.id) AS activities_count").joins(:activities).group("users.id").order("activities_count DESC").limit(15) }

  # select("users.id, users.name, count(activities.id) AS activities_count")
  def friendsLeaderboard
    return self.friends.joins(:activities).group("users.id").order("count(activities.id) DESC").limit(15)
  end

  def isFriend(other)
    friendship = Friendship.where(user_id: self.id, friend_id: other.id, status: 2).first
    return friendship
  end

  def isPendingFriend(other)
    friendship = Friendship.where(user_id: self.id, friend_id: other.id, status: 0).first
    return friendship
  end

  def isRequestedFriend(other)
    friendship = Friendship.where(user_id: self.id, friend_id: other.id, status: 1).first
    return friendship
  end

  def isTeamOwner(other)
    owner = Team.where(user_id: self.id, id: other.id).first
    return owner
  end

  def isChallengeOwner(other)
    owner = Challenge.where(user_id: self.id, id: other.id).first
    return owner
  end

  def age
    age = Time.now.year-self.birth_date.year
    if (self.birth_date.month>Time.now.month)
      age = age-1
    else
      if (self.birth_date.month==Time.now.month)
        if (self.birth_date.day>Time.now.day)
          age = age-1
        end
      end
    end

    return age
  end

  def isAdmin?
    return self.access_level>=2
  end

  def bmi
    return self.weight/(self.height*self.height)
  end

  def caloriesPerDay
    calories = 10*self.weight+6.25*self.height*100-5*self.age
    if self.gender=="Male"
      calories = calories+5
    else
      calories = calories-161
    end

    return calories
  end
end
