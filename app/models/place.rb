class Place < ActiveRecord::Base
  has_many :activities
  has_many :challenges
  has_many :users, through: :activities

  validates :name, :coordinate_lat, :coordinate_long, presence: true
  validates :name, uniqueness: true


end
