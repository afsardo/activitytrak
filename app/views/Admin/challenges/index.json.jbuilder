json.array!(@challenges) do |challenge|
  json.extract! challenge, :id, :name, :start_date, :final_date, :max_tries, :user_id, :place_id, :activity_type_id
  json.url challenge_url(challenge, format: :json)
end
