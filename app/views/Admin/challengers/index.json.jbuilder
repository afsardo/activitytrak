json.array!(@challengers) do |challenger|
  json.extract! challenger, :id, :challenge_id, :user_id, :status
  json.url challenger_url(challenger, format: :json)
end
