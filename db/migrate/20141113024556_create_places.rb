class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :name, null: false
      t.decimal :coordinate_lat, null: false
      t.decimal :coordinate_long, null: false

      t.timestamps
    end
  end
end
