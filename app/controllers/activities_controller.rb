class ActivitiesController < ApplicationController
  respond_to :html, :json
  before_action :set_activity, only: [:show, :edit, :update, :destroy]

  def index
    @activities = @current_user.activities.order('final_date DESC')
    respond_with(@activities)
  end

  def show
    @activity_object = JSON.parse(@activity.json_object)
    if @activity_object
      if @activity_object["distance"]
        @activity_object["avg_velocity"] = (@activity_object["distance"]/1000) / ((@activity.final_date-@activity.start_date).seconds / 1.hour)
      end
    end
    respond_with(@activity, @activity_object)
  end

  def new
    @activity = Activity.new
    respond_with(@activity)
  end

  def edit
  end

  def create
    @activity = Activity.new(activity_params)
    @activity.user_id = @current_user.id
    @activity.json_object = "{}"
    if params[:activity][:challenge_id]
      @activity.challenge_id = params[:activity][:challenge_id]
    end
    @activity.save
    @event = Event.new(user_id: @current_user.id, object_type: 1, object_id: @activity.id, action: "#{@current_user.name} created an activity!")
    @event.save
    respond_with(@activity)
  end

  def update
    @activity.update(activity_params)
    if params[:activity][:challenge_id]
      @activity.update(challenge_id: params[:activity][:challenge_id])
    end
    respond_with(@activity)
  end

  def destroy
    @activity.destroy
    respond_with(@activity)
  end

  private
    def set_activity
        @activity = Activity.find(params[:id])
    end

    def activity_params
      params.require(:activity).permit(:name, :activity_type_id, :place_id, :start_date, :final_date)
    end
end
