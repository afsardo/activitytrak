class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :is_logged_in
  before_action :has_account
  before_action :set_current_user

  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  def after_sign_in_path_for(login)
    dashboard_path
  end

  private

  def record_not_found
    redirect_to not_found_path
    return nil
  end

  def is_logged_in
    unless login_signed_in?
      redirect_to root_path
      return nil
    end
  end

  def has_account
    if login_signed_in?
      unless current_login.user
        redirect_to new_user_path
        return nil
      end
    end
  end

  def set_current_user
    if login_signed_in?
      if current_login.user
        @current_user = current_login.user
      end
    end
  end

end
