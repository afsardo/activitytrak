class WelcomeController < ApplicationController

  skip_before_action :is_logged_in, only: [:index, :search, :leaderboards, :popular_sports, :popular_places, :contact, :about, :privacy]

  def index
    if login_signed_in?
      redirect_to livefeed_path
    end
  end

  def search
    if params[:value].blank?
      redirect_to root_path
    end

    @users = User.where("name LIKE ?","%"+params[:value]+"%")
    @places = Place.where("name LIKE ?","%"+params[:value]+"%")
    @teams = Team.where("name LIKE ?","%"+params[:value]+"%")
  end

  def livefeed
    @feed = []
    if Event.all.size != 0
      @livefeed = Event.all.order(created_at: :desc).select { |event| event.user.isFriend(@current_user) }
      @livefeed = @livefeed[0 .. 9]

      @livefeed.each do |event|
        if event.object_type == 1
          @feed.push([event, Activity.find(event.object_id)])
        end
        if event.object_type == 2
          @feed.push([event, Team.find(event.object_id)])
        end
        if event.object_type == 3
          @feed.push([event, Challenge.find(event.object_id)])
        end
        if event.object_type == 4
          @feed.push([event, Challenger.find(event.object_id)])
        end
        if event.object_type == 5
          @feed.push([event, TeamMember.find(event.object_id)])
        end
      end
    end
  end

  def leaderboards
    @users_leaderboard = User.leaderboard
    if login_signed_in?
      @users_leaderboard = @current_user.friendsLeaderboard
    end
    @teams_leaderboard = Team.leaderboard
  end

  def popular_sports
    @popular_sports = ActivityType.popular_sports
  end

  def popular_places
    @sports = ActivityType.all
  end

  def contact

  end

  def about

  end

  def privacy

  end

  def access_denied

  end

  def not_found

  end
end
