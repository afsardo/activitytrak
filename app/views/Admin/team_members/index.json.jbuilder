json.array!(@team_members) do |team_member|
  json.extract! team_member, :id, :team_id, :user_id, :status, :date
  json.url team_member_url(team_member, format: :json)
end
