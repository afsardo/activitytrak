class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name, null: false
      t.string :tag, null: false
      t.string :logo
      t.integer :user_id, null: false

      t.timestamps
    end

    add_index :teams, :user_id
  end
end
