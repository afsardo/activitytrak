class CreateTeamMembers < ActiveRecord::Migration
  def change
    create_table :team_members do |t|
      t.integer :team_id, null: false
      t.integer :user_id, null: false
      t.integer :status, default: 0
      t.datetime :date, default: Time.now

      t.timestamps
    end

    add_index :team_members, [:team_id, :user_id]
  end
end
