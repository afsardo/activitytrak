class CreateFriendships < ActiveRecord::Migration
  def change
    create_table :friendships do |t|
      t.integer :user_id, null: false
      t.integer :friend_id, null: false
      t.integer :status, default: 0
      t.datetime :date, default: Time.now

      t.timestamps
    end

    add_index :friendships, [:user_id, :friend_id]
  end
end
