json.array!(@activities) do |activity|
  json.extract! activity, :id, :name, :activity_type_id, :user_id, :place_id, :start_date, :final_date, :json_object, :challenge_id
  json.url activity_url(activity, format: :json)
end
