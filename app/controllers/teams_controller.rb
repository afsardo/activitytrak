class TeamsController < ApplicationController
  respond_to :html, :json
  skip_before_action :is_logged_in, only: [:show]
  before_action :set_team, only: [:show, :edit, :update, :destroy]

  def index
    @teams = @current_user.teams
    @teams_pending = @current_user.teams_pending
    respond_with(@teams,@teams_pending)
  end

  def show
    respond_with(@team)
  end

  def new
    @team = Team.new
    respond_with(@team)
  end

  def edit
  end

  def create
    @team = Team.new(team_params)
    @team.user_id = @current_user.id
    @team.save
    @event = Event.new(user_id: @current_user.id, object_type: 2, object_id: @team.id, action: "#{@current_user.name} created a team!")
    @event.save

    if @team.save
      team_member = TeamMember.new
      team_member.team_id = @team.id
      team_member.user_id = @current_user.id
      team_member.status = 1
      team_member.save
    end
    respond_with(@team)
  end

  def update
    @team.update(team_params)
    respond_with(@team)
  end

  def destroy
    @team.destroy
    respond_with(@team)
  end

  private
    def set_team
      @team = Team.find(params[:id])
    end

    def team_params
      params.require(:team).permit(:name, :tag, :logo)
    end
end
