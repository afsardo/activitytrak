class CreateChallengers < ActiveRecord::Migration
  def change
    create_table :challengers do |t|
      t.integer :challenge_id, null: false
      t.integer :user_id, null: false
      t.integer :status, default: 0

      t.timestamps
    end

    add_index :challengers, [:challenge_id, :user_id]
  end
end
