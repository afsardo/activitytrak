json.array!(@places) do |place|
  json.extract! place, :id, :name, :coordinate_lat, :coordinate_long
  json.url place_url(place, format: :json)
end
