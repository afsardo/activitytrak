class Admin::ChallengersController < AdminController
  before_action :set_challenger, only: [:show, :edit, :update, :destroy]

  def index
    @challengers = Challenger.all
    respond_with(@challengers)
  end

  def show
    respond_with(@challenger)
  end

  def new
    @challenger = Challenger.new
    respond_with(@challenger)
  end

  def edit
  end

  def create
    @challenger = Challenger.new(challenger_params)
    @challenger.save
    respond_with(@challenger)
  end

  def update
    @challenger.update(challenger_params)
    respond_with(@challenger)
  end

  def destroy
    @challenger.destroy
    respond_with(@challenger)
  end

  private
    def set_challenger
      @challenger = Challenger.find(params[:id])
    end

    def challenger_params
      params.require(:challenger).permit(:challenge_id, :user_id, :status)
    end
end
