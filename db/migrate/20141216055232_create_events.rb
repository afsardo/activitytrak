class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |e|
      e.integer :object_type, null: false
      e.integer :object_id, null: false
      e.integer :user_id, null: false
      e.string :action, null: false

      e.timestamps
    end

    add_index :events, [:user_id, :object_id]
  end
end