class FriendshipsController < ApplicationController
  respond_to :html, :json

  def index
    @friendships = @current_user.friendships
    @pending_friends = @current_user.pending_friends
    @request_friends = @current_user.requested_friends
    respond_with(@friendships)
  end

  def show
      user = User.find(params[:id])
      friendship = Friendship.new
      friendship.user_id = @current_user.id
      friendship.friend_id = user.id
      friendship.status = 1
      friendship.save

      otherEndFriendship = Friendship.new
      otherEndFriendship.user_id = user.id
      otherEndFriendship.friend_id = @current_user.id
      otherEndFriendship.status = 0
      otherEndFriendship.save

      redirect_to user_path user
  end

  def new
  end

  def edit
    @friendship = Friendship.find(params[:id])
    if (@friendship.status!=0)
      redirect_to access_denied_path
      return nil
    end

    date = Time.now
    @friendship.status = 2
    @friendship.date = date
    @friendship.save

    otherEndFriendship = Friendship.where(user_id: @friendship.friend_id, friend_id: @friendship.user_id).first
    otherEndFriendship.status = 2
    otherEndFriendship.date = date
    otherEndFriendship.save


    redirect_to friendships_path
  end

  def create
  end

  def update
  end

  def destroy
    @friendship = Friendship.find(params[:id])
    @friendship.destroy
    otherEndFriendship = Friendship.where(user_id: @friendship.friend_id, friend_id: @friendship.user_id).first
    otherEndFriendship.destroy
    respond_with(@friendship)
  end
end
