class Admin::ChallengesController < AdminController
  before_action :set_challenge, only: [:show, :edit, :update, :destroy]

  def index
    @challenges = Challenge.all
    respond_with(@challenges)
  end

  def show
    respond_with(@challenge)
  end

  def new
    @challenge = Challenge.new
    respond_with(@challenge)
  end

  def edit
  end

  def create
    @challenge = Challenge.new(challenge_params)
    @challenge.save
    respond_with(@challenge)
  end

  def update
    @challenge.update(challenge_params)
    respond_with(@challenge)
  end

  def destroy
    @challenge.destroy
    respond_with(@challenge)
  end

  private
    def set_challenge
      @challenge = Challenge.find(params[:id])
    end

    def challenge_params
      params.require(:challenge).permit(:name, :start_date, :final_date, :max_tries, :user_id, :place_id, :activity_type_id)
    end
end
