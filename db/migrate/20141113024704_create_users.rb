class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :photo
      t.date :birth_date, null: false
      t.string :town
      t.string :gender, null: false
      t.decimal :weight, null: false
      t.decimal :height, null: false
      t.string :profile_link
      t.integer :access_level, default: 1
      t.integer :login_id

      t.timestamps
    end

    add_index :users, :login_id, unique: true
  end
end
