class Challenge < ActiveRecord::Base
  has_many :challengers, dependent: :destroy
  has_many :challenger_users, -> { where('challengers.status = 1') }, through: :challengers, source: :user
  belongs_to :user
  belongs_to :place
  belongs_to :activity_type

  validates :name, :start_date, :final_date, :place_id, :activity_type_id, presence: true
  validates :max_tries, numericality: { only_integer: true, greater_than_or_equal_to: 1 }
  validates :name, uniqueness: true

  def isCompetitor?(user)
    challenger = Challenger.where(user_id: user.id, challenge_id: self.id, status: 1).first
    return challenger
  end

  def isPendingCompetitor?(user)
    challenger = Challenger.where(user_id: user.id, challenge_id: self.id, status: 0).first
    return challenger
  end

  def isOver
    return self.final_date<=Time.now
  end

  def isWinner(user)
    challengers = Challenger.where(user_id: user.id, challenge_id: self.id, status: 1)
    return true && self.isOver
  end
end
