class PlacesController < ApplicationController
  respond_to :html, :json
  before_action :set_place, only: [:show]
  skip_before_action :is_logged_in, only: [:show]

  def index
    @places = Place.all
    respond_with(@places)
  end

  def show
    respond_with(@place)
  end

  def new
    @place = Place.new
    respond_with(@place)
  end

  def create
    @place = Place.new(place_params)
    @place.save
    respond_with(@place)
  end

  private
    def set_place
      @place = Place.find(params[:id])
    end

    def place_params
      params.require(:place).permit(:name, :coordinate_lat, :coordinate_long)
    end
end
