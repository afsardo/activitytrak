class Event < ActiveRecord::Base
  belongs_to :user

  validates :object_type, :object_id, :user_id, :action, presence: true

end
