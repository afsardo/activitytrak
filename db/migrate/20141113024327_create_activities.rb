class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :name, null: false
      t.integer :activity_type_id, null: false
      t.integer :user_id, null: false
      t.integer :place_id, null: false
      t.datetime :start_date
      t.datetime :final_date
      t.text :json_object
      t.integer :challenge_id

      t.timestamps
    end

    add_index :activities, [:activity_type_id, :user_id, :place_id]
  end
end
